#!/bin/sh

git pull
mix deps.get
mix compile
echo "Don't forget to run 'systemctl restart pleroma.service'!"
